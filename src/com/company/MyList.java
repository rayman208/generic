package com.company;

public class MyList<T>
{
    private T[] array;

    public MyList()
    {
        array = (T[])new Object[0];
    }

    private void ResizeArray(int newLength)
    {
        T[] newArray = (T[])new Object[newLength];

        int actualLength = newLength<array.length ? newLength : array.length;

        for (int i = 0; i < actualLength; i++) {
            newArray[i]=array[i];
        }

        array = newArray;
    }

    public void Add(T elem)
    {
        ResizeArray(array.length+1);
        array[array.length-1]=elem;
    }

    public void RemoveAt(int index)
    {
        if(array.length>0 && index>=0 && index<=array.length-1)
        {
            for (int i = index; i < array.length-1; i++)
            {
                array[i]=array[i+1];
            }

            ResizeArray(array.length-1);
        }
    }

    public void InsertAt(int index, T elem)
    {
        if(array.length>0 && index>=0 && index<=array.length-1)
        {
            ResizeArray(array.length+1);

            for (int i = array.length-1; i > index ; i--)
            {
                array[i]=array[i-1];
            }

            array[index] = elem;
        }
    }

    public void Clear()
    {
        array = (T[])new Object[0];
    }

    public T GetAt(int index)
    {
        try
        {
            return array[index];
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    public void SetAt(int index, T elem)
    {
        if(array.length>0 && index>=0 && index<=array.length-1)
        {
            array[index] = elem;
        }
    }

    public int Length()
    {
        return array.length;
    }

}
