package com.company;

import javax.swing.*;

public class Main {

    static void PrintList(MyList list)
    {
        for (int i = 0; i < list.Length(); i++)
        {
            System.out.print(list.GetAt(i)+" ");
        }
        System.out.println();
    }


    public static void main(String[] args)
    {
        MyList<Cat> list = new MyList<Cat>();
        list.Add(new Cat("boris"));
        list.Add(new Cat("oleg"));
        list.Add(new Cat("vasya"));

        PrintList(list);

        /*list.SetAt(0,999);
        list.InsertAt(1,111);

        PrintList(list);

        list.RemoveAt(2);

        PrintList(list);*/
    }
}
